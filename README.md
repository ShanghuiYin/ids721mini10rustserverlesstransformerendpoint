
# IDS721 Spring 2024 Weekly Mini Project 10 Rust Serverless Transformer Endpoint

## Requirements
1. Dockerize Hugging Face Rust transformer
2. Deploy container to AWS Lambda
3. Implement query endpoint


## Setup
1. Install Rust
2. Start by creating a new binary-based Cargo project and changing into the new directory:
   `cargo new project_name && cd project_name`
3. add the dependencies in `Cargo.toml` file for cli and testing
    ```toml
    [dependencies]
    lambda_runtime = "0.9.1"
    openssl = { version = "0.10.35", features = ["vendored"] }
    rust-bert = "0.22.0"
    tracing = { version = "0.1", features = ["log"] }
    tracing-subscriber = { version = "0.3", default-features = false, features = ["env-filter", "fmt"] }
    tch = "0.14.0"
    anyhow = "1.0.40"
    lambda_http = "0.11.1"
    tokio = { version = "1", features = ["macros", "rt-multi-thread"] }
    serde = {version = "1.0", features = ["derive"] }
    serde_json = "1.0"
    llm = { git = "https://github.com/rustformers/llm" , branch = "main" }
    rand = "0.8.5"
   ```
4. Implement the logic in `main.rs` file
6. Run the project locally `cargo lambda watch`

## Deployment
1. Install the AWS CLI
2. Build the project to AWS Lambda `cargo lambda build --release`
3. Retrieve an authentication token and authenticate your Docker client to your registry.
   `aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin xxx`
4. Build your Docker image using the following command.
   `docker build -t rust .`
5. After the build completes, tag your image so you can push the image to this repository
   `docker tag rust:latest xxx/rust:latest`
6. Run the following command to push this image to your newly created AWS repository
   `docker push xxx/rust:latest`

![img_1.png](img_1.png)

## Demo

Test the endpoint using the following command

`curl -X POST https://k4f7g8z3qi.execute-api.us-east-1.amazonaws.com/default/rust10 -d '{"text":"What is Rust?"}'`
   
The output should be as follows:
   ```json
   {"result":"Rust was designed to be a language that focuses on..."}
   ```

![img.png](img.png)


## Reference
1. https://www.cargo-lambda.info/guide/getting-started.html
2. https://aws.amazon.com/cn/blogs/compute/hosting-hugging-face-models-on-aws-lambda/
3. https://docs.aws.amazon.com/lambda/latest/dg/images-create.html
4. https://docs.rs/rust-bert/latest/rust_bert/
5. https://docs.rs/llm/latest/llm/
6. https://cjwebb.com/aws-lambda-with-rust/

